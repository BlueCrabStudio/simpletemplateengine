package com.bluecrabstudio.buildscript

import org.gradle.api.GradleException

class VersionHandler {
    private def major = 0
    private def minor = 0
    private def patch = 0
    private def appendix = ""

    VersionHandler(version) {
        parse(version.toString())
    }

    def increase() {
        def version = "${major}.${minor}.${++patch}"
        if (!appendix.isEmpty()) version = "$version-$appendix"
        def gradleProperties = new Properties()
        File propertiesFile = new File("gradle.properties")
        if (!propertiesFile.exists()) {
            propertiesFile = new File("../gradle.properties")
        }
        try {
            gradleProperties.load(propertiesFile.newDataInputStream())
            gradleProperties.setProperty("version", version)
            gradleProperties.store(propertiesFile.newWriter(), "Version is now: ${version}")
        } catch (def ioException) {
            throw new GradleException("Properties file cannot be stored/loaded! path: ${propertiesFile.getAbsolutePath()}", ioException)
        }
    }

    def parse(String ver) {
        def splittedVersion = ver.split("[-.]")
        if (splittedVersion.length < 3 || splittedVersion.length > 4) {
            throw new GradleException("Version is invalid! expected format \"major.minor.patch-appendix\" (e.g. 1.0.0-SNAPSHOT or 2.1.0), but found $splittedVersion")
        }
        major = Integer.parseInt(splittedVersion[0].trim())
        minor = Integer.parseInt(splittedVersion[1].trim())
        patch = Integer.parseInt(splittedVersion[2].trim())
        appendix = (splittedVersion.length == 4) ? splittedVersion[3].trim(): ""
        def correctMajor = ((major instanceof Integer) && major >= 0)
        def correctMinor = ((minor instanceof Integer) && minor >= 0)
        def correctPatch = ((patch instanceof Integer) && patch >= 0)
        def correctAppendix = (appendix != null && (appendix instanceof String))
        if (!correctMajor) {
            throw new GradleException("Major is incorrect! Found '" + major + "'")
        }
        if (!correctMinor) {
            throw new GradleException("Minor is incorrect! Found '" + minor + "'")
        }
        if (!correctPatch) {
            throw new GradleException("Patch is incorrect! Found '" + patch + "'")
        }
        if (!correctAppendix) {
            throw new GradleException("Appendix is incorrect! Found '" + appendix + "'")
        }
    }
}