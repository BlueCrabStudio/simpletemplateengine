package com.bluecrabstudio.tpldemo;

import java.util.logging.Logger;

public class MainApplication {
    private static final Logger LOGGER = Logger.getLogger(MainApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Executing main with args:\n" + String.join(",\n", args) + "\n---------------------------------------------------------");
        final MatcherConfiguration matcherConfiguration = new ArgumentParser().parse(args);
        TemplateProcessor processor = matcherConfiguration.createTemplateProcessor();
        processor.processAllTemplates();
    }
}
