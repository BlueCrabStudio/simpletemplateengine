package com.bluecrabstudio.tpldemo;

import java.io.File;
import java.util.logging.Logger;

public class ArgumentParser {
    private static final Logger LOGGER = Logger.getLogger(ArgumentParser.class.getName());

    public MatcherConfiguration parse(String[] arguments) {
        switch (arguments.length) {
            case 2:
            case 3:
                return parseSimpleCall(arguments);
            case 4:
            case 6:
                return parseRegularCall(arguments);
            default:
                printHelp();
        }
        throw new IllegalArgumentException("Arguments could not be parsed!");
    }

    private MatcherConfiguration parseRegularCall(String[] arguments) {
        // TODO: implement me!
        return null;
    }

    private MatcherConfiguration parseSimpleCall(String[] arguments) {
        final File templatePath = parseFile(arguments[0], "template", true);
        final File dataModelFile = parseFile(arguments[1], "data model", false);
        final File outputPath = arguments[2] != null ? parseFile(arguments[2], "output", true) : templatePath;
        return new MatcherConfiguration(templatePath, dataModelFile, outputPath);
    }

    private File parseFile(final String path, final String name, final boolean shouldBeDirectory) {
        if (path == null || path.isBlank()) {
            throw new IllegalArgumentException("The given " + name + " path '" + path + "' is invalid!");
        }
        final File file = new File(path.trim());
        if (file.isDirectory() != shouldBeDirectory) {
            throw new IllegalArgumentException("The given " + name + " path should be a directory: " + shouldBeDirectory + " - Actual is: " + file.isDirectory());
        }
        return file;
    }

    private void printHelp() {
        LOGGER.info(
                "Usage of the cli:\n" +
                        "   regular parameters\n" +
                        "   --templatedirectory absolute/path/to/template/folder --datamodel absolute/path/to/json/model.json --outputdirectory absolute/path/to/output/folder\n" +
                        "\n" +
                        "   shortened parameters\n" +
                        "   -td absolute/path/to/template/folder -dm absolute/path/to/json/model.json -od absolute/path/to/output/folder\n" +
                        "\n" +
                        "   simplest parameters\n" +
                        "   absolute/path/to/template/folder absolute/path/to/json/model.json\n" +
                        "-----------------------------------------------------------------\n" +
                        "   Notes:\n" +
                        "      The order of the named parameters is irrelevant\n" +
                        "      The order of the of the unnamed parameters is always\n" +
                        "          1. template directory\n" +
                        "          2. data model\n" +
                        "          3. output directory (optional)\n" +
                        "      The data model is always a valid json file\n" +
                        "      The output directory parameter is optional\n" +
                        "      Without the output directory, the output will be located in the template directory"
        );
    }
}

/**
 * "C:\Program Files\AdoptOpenJDK\jdk-14.0.0.36-hotspot\bin\java.exe"
 * "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2019.3.2\lib\idea_rt.jar=61472:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2019.3.2\bin"
 * -Dfile.encoding=UTF-8 -classpath
 * E:\projects\TemplateEngineDemo\cli\build\classes\java\main;
 * E:\projects\TemplateEngineDemo\cli\build\resources\main
 * com.bluecrabstudio.tpldemo.MainApplication
 * E:\projects\TemplateEngineDemo\cli\src\main\resources\templates
 * E:\projects\TemplateEngineDemo\cli\src\main\resources\datamodel\datamodel2.properties
 * E:\projects\TemplateEngineDemo\cli\src\main\resources\output
 */
