package com.bluecrabstudio.tpldemo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Logger;

public class DataModel {
    private static final Logger LOGGER = Logger.getLogger(DataModel.class.getName());
    private final File dataModelFile;
    private String dataModelContent;

    DataModel(final File dataModelFile) {
        this.dataModelFile = dataModelFile;
        dataModelContent = readContentFromDataModelFile();
    }

    private String readContentFromDataModelFile() {
        try {
            return new String(Files.readAllBytes(dataModelFile.toPath()));
        } catch (IOException e) {
            LOGGER.severe("Could not get template file with name: ");
            throw new IllegalStateException(e.getMessage());
        }
    }

    public String getDataModelFileExtension() {
        return dataModelFile.getName().substring(dataModelFile.getName().lastIndexOf('.') + 1);
    }

    public String getContentFromDataModel() {
        return dataModelContent;
    }

    public File getDataModelFile() {
        return dataModelFile;
    }

    public void setDataModelContent(String dataModelContent) {
        this.dataModelContent = dataModelContent;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "dataModelFile=" + dataModelFile +
                '}';
    }
}
