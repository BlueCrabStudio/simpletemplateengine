package com.bluecrabstudio.tpldemo;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class MatcherConfiguration {
    private static final Logger LOGGER = Logger.getLogger(MatcherConfiguration.class.getName());
    private final Configuration config;
    private final File templatePath;
    private final File outputPath;
    private final DataModel dataModel;

    public MatcherConfiguration(File templatePath, File dataModelFile, File outputPath) {
        this.templatePath = templatePath;
        this.dataModel = new DataModel(dataModelFile);
        this.outputPath = outputPath;
        try {
            config = new Configuration();
            config.setDefaultEncoding("UTF-8");
            config.setDirectoryForTemplateLoading(templatePath);
        } catch (IOException e) {
            LOGGER.severe("Could not apply configuration settings!");
            throw new IllegalStateException(e.getMessage());
        }
        LOGGER.info("Successfully created configuration!");
        LOGGER.config(toString());
    }

    public TemplateProcessor createTemplateProcessor() {
        LOGGER.fine("Created template processor!");
        return new TemplateProcessor(this);
    }

    public String[] getTemplateFileNames() {
        final String[] list = templatePath.list((dir, name) -> name.contains(".template"));
        return Objects.requireNonNull(list);
    }

    public Template getTemplate(String templateFileName) {
        try {
            return config.getTemplate(templateFileName);
        } catch (IOException e) {
            LOGGER.severe("Could not get template file with name: " + templateFileName);
            throw new IllegalStateException(e.getMessage());
        }
    }

    public DataModel getDataModel() {
        return dataModel;
    }

    @Override
    public String toString() {
        return "MatcherConfiguration{" +
                "templatePath=" + templatePath +
                ", outputPath=" + outputPath +
                ", dataModelFile=" + dataModel +
                '}';
    }

    public String getOutputPath() {
        return outputPath.getPath()+"//";
    }
}
