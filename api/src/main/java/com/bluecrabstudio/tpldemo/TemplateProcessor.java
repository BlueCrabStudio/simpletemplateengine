package com.bluecrabstudio.tpldemo;

import com.bluecrabstudio.tpldemo.datamodelparser.DataModelParser;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;

public class TemplateProcessor {
    private static final Logger LOGGER = Logger.getLogger(TemplateProcessor.class.getName());
    private final MatcherConfiguration matcherConfiguration;

    public TemplateProcessor(MatcherConfiguration matcherConfiguration) {
        this.matcherConfiguration = matcherConfiguration;
    }

    public void processAllTemplates() {
        LOGGER.fine("Started processing templates!");
        Map<Object, Object> dataModel = DataModelParser.getParser(matcherConfiguration.getDataModel()).parseDataModel();
        Arrays.stream(matcherConfiguration.getTemplateFileNames()).forEach(templateFileName -> injectModelIntoTemplate(dataModel, templateFileName));
        LOGGER.info("Finished processing templates!");
    }

    private void injectModelIntoTemplate(Map<Object, Object> dataModel, String templateFileName) {
        LOGGER.finest("Processing template with name: " + templateFileName);
        final Template template = matcherConfiguration.getTemplate(templateFileName);
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(getOutputFileName(templateFileName)));
            template.process(dataModel, writer);
            writer.close();
        } catch (IOException | TemplateException e) {
            LOGGER.severe("Could not inject data model into template!");
            throw new IllegalStateException(e.getMessage());
        }
    }

    private String getOutputFileName(final String inputFileName) {
        return matcherConfiguration.getOutputPath() + inputFileName.replace(".template", "");
    }
}
