package com.bluecrabstudio.tpldemo.datamodelparser;

import com.bluecrabstudio.tpldemo.DataModel;

import java.util.Map;

public interface Parsable {
    Map<Object, Object> parseDataModel(final DataModel dataModel);
}
