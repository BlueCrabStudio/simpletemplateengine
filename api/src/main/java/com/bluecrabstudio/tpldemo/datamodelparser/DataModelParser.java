package com.bluecrabstudio.tpldemo.datamodelparser;

import com.bluecrabstudio.tpldemo.DataModel;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.yaml.snakeyaml.Yaml;
import pl.jalokim.propertiestojson.util.PropertiesToJsonConverter;

import java.util.Map;
import java.util.logging.Logger;

public enum DataModelParser implements Parsable {
    JSON("json") {
        @Override
        public Map<Object, Object> parseDataModel(final DataModel dataModel) {
            try {
                final String contentFromDataModel = dataModel.getContentFromDataModel();
                return new Gson().fromJson(contentFromDataModel, Map.class);
            } catch (JsonSyntaxException e) {
                LOGGER.severe("Could not create data model!");
                throw new IllegalStateException(e.getMessage());
            }
        }
    },

    PROPERTIES("properties") {
        @Override
        public Map<Object, Object> parseDataModel(final DataModel dataModel) {
            final String json = new PropertiesToJsonConverter().convertPropertiesFromFileToJson(dataModel.getDataModelFile());
            dataModel.setDataModelContent(json);
            return JSON.parseDataModel(dataModel);
        }
    },

    YAML("yaml") {
        @Override
        public Map<Object, Object> parseDataModel(final DataModel dataModel) {
            return new Yaml().load(dataModel.getContentFromDataModel());
        }
    };

    private final String fileExtension;
    private static final Logger LOGGER = Logger.getLogger(DataModelParser.class.getName());
    private static DataModel dataModel;

    DataModelParser(final String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public static DataModelParser getParser(final DataModel dataModel) {
        for (DataModelParser type: DataModelParser.values()) {
            if (type.getFileExtension().equals(dataModel.getDataModelFileExtension())) {
                DataModelParser.dataModel = dataModel;
                return type;
            }
        }
        final IllegalArgumentException illegalArgumentException = new IllegalArgumentException("DataModel file with extension '" + dataModel.getDataModelFileExtension() + "' is not supported yet!");
        LOGGER.severe(illegalArgumentException.getMessage());
        throw illegalArgumentException;
    }

    public Map<Object, Object> parseDataModel() {
        if (dataModel == null) {
            final UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException("DataModel is null! Use DataModelParser.getParser(DataModel) or parseDataModel(DataModel)!");
            LOGGER.severe(unsupportedOperationException.getMessage());
            throw unsupportedOperationException;
        }
        return parseDataModel(dataModel);
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
