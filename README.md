# Template Engine Demo
## Description
This is a demo project to show and learn the usage of the freemarker template engine.
The demo needs a data-model and a given set of templates.
The demo will then inject the model into templates. All freemarker tags will be replaced by the data model content.
For more information about freemarker templates, click [here]("https://freemarker.apache.org/docs/dgui_quickstart.html").

![templates + data-model = output!](/img/tpl-ngn-demo.png)

---

## Usage

The application needs three parameters:
1. **The template files to use**
    - Provide a folder with *.template.** files
    - E.g. *path/to/my/templates* with an *index.template.html* file
    - This folder may contain as many *.template.** files as you want
    - Only *.template.** files will be processed

2. **The data-model to inject**
    - Provide a single file with the data-model
    - The model may be a *.properties*, *.yaml* or a *.json* file
    
3. **The name of the output directory**
    - This parameter is optional
    - If left empty, the output will be placed into the mandatory template directory

## Build the application
Via `gradle run` the cli is called with following default parameters:

* **template folder** = `cli/src/main/resources/templates`
* **data-model file** = `cli/src/main/resources/datamodel/datamodel1.json`
* **output folder** = `cli/src/main/resources/output`

Feel free to change the parameters inside the `run` configuration in file `cli/build.gradle`

## Agenda (TODO)
### Milestones
* Deploy api as open source library on maven central
* Cli project should build an executable fat jar
* Create SpringBoot application to expose features as RESTapi
* Support XML files as data model
* Create Frontend PWA
* Bundle application as Docker container
* Interface for custom DataModelProvider
* Example Project

### Project Depts
* Write user centric documentation
* Add changelog
* Setup CI/CD
* Setup contribution guide
* Support more cli parameter permutations
* Add help command
* Add health check
* Improve logging
* More example usages

## Known issues
TBD

## License
[Apache License - Version 2.0](https://choosealicense.com/licenses/apache-2.0/)



